<?php 
    include 'checkip.php';
    $ch = new CheckIP();
    //Example ip for localhost 
    //$user_ip = "1.1.1.198";
    $user_ip = $ch->getUserIP();
    $inicial = $user_ip[0];
    $ch->validateIP($inicial, "lavadoras-inicio", $user_ip);
?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Servicio aLavadoras en Ciudad de México. Servicio Urgente a Domicilio en 2hrs. Centro de Servicio Especialista. Reparación, Mantenimiento e Instalación de Refrigeradores, Estufas, Lavadoras, Centros de Lavado, Hornos. Teléfono: 5565020417">
  <meta name="Keywords" content="Reparación de Refrigeradores Frigidaire, Reparación de Lavadoras Samsung, Reparación de Lavavajillas Frigidaire, Reparación de Secadoras Frigidaire, Servicio de Linea Blanca Frigidaire, Servicio de Refrigeradores Frigidaire, Servicio de Lavadoras Frigidaire, Servicio de Lavavajillas Frigidaire, Servicio Tecnico Frigidaire, compostura de refrigeradores, Reparación de lavadoras, reparación de refrigeradores, centro de servicio para lavadoras, servicio frigidaire, servicio samsung, servicio maytag, servicio mabe, servicio whirlpool, servicio torrey, servicio wolf, servicio a lavadoras urgente, Servicio Frigidaire en CDMX, reparacion de lavadoras, servicio para lavadoras, servicio para lavadoras a domicilio, servicio a domicilio para estufas, reparamos estufas a domicilio, tecnicos de lavadoras, tecnicos de lavadoras a domicilio, servicio para lavadoras, servicio a lavadoras, reparacion de lavadoras, reparamos lavadoras, servicio a lavadoras a domicilio, servicio para lavadoras en cdmx, service samsung">
  <meta name="title" content="Servicio a Lavadoras en CDMX | Técnicos Especialistas | -10% de Descuento">
  <meta name="author" content="Páginas Web Premium">
  <meta name="Subject" content="Servicio Frigidaire en CDMX">
  <meta name="Language" content="es">
  <meta name="Revisit-after" content="15 days">
  <meta name="Distribution" content="Global">
  <meta name="Robots" content="Index, follow">
  <title>Servicio para Lavadoras en CDMX | Técnicos Especialistas | -10% de Descuento</title>

  <!-- Bootstrap core CSS -->
  <link href="dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <link href="carousel.css" rel="stylesheet" crossorigin="anonymous">

  <!-- Favicons -->
  <link rel="apple-touch-icon" href="assets/img/logo-servicio-frigidaire-en-cdmx.png" sizes="180x180">
  <meta name="theme-color" content="#000">

  <!-- Custom styles for this template -->
  <link href="album.css" rel="stylesheet">
  
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165139576-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-165139576-1');
  </script>

  <!-- Google Analytics Events-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-165139576-1', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" async></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>


<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 bg-white border-bottom shadow-sm">

  <img src="assets/img/reparacion-de-refrigeradores-cdmx-logo.png" width="160px" alt="reparacion de lavadoras" class="my-0 mr-md-auto font-weight-normal">

  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Reparación</a>
    <a class="p-2 text-dark btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Reparación</a>
    <a class="p-2 text-dark btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Mantenimiento</a>
    <a class="p-2 text-dark btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Mantenimiento</a>
    <a class="p-2 text-dark btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Contacto</a>
    <a class="p-2 text-dark btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Contacto</a>
  </nav>
  <a class="btn btn-outline-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Agendar Visita</a>
  <a class="btn btn-primary btn-mobile-action btn-block" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Agendar Visita</a>
</div>

<main role="main">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active pwp-background-third pwp-background-third-mobile">
        <div class="container ">
          <div itemscope itemtype="http://schema.org/Dataset"  class="carousel-caption text-left">
            <h2 class="tittle-h2"><b itemprop="name">Servicio a Lavadoras</b></h2>
            <p class="text"><span itemprop="description">Contamos con técnicos especializados y servicio a dimicilio en la CDMX y Área Metropolitana, todos nuestros técnicos son capaces de resolver cualquier problema que tenga tu lavadora lo más rápido posible.</span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')" >Enviar Mensaje</a>
              <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" role="button" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a>
            </p>
          </div>
        </div> 
      </div>
      
      <div class="carousel-item pwp-background-first pwp-background-first-mobile">
        <div class="container">
          <div class="carousel-caption text-right">
            <h2 class="tittle-h2"><b>Servicio a Refrigeradores</b></h2>
            <p class="text">Al contar con más de 20 años de experiencia brindamos servicios a Refrigeradores Comerciales e Industriales, Cargas de Gas y Limpieza de Ductos.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Enviar Mensaje</a>
              <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item pwp-background-four pwp-background-four-mobile">
          <div class="container">
            <div class="carousel-caption text-left">
              <h2 class="tittle-h2"><b>Servicio a Estufas</b></h2>
              <p class="text">El funcionamiento de los refrigeradores se centra en un ciclo de refrigeración en constante movimiento, asi es que mantiene las cosas frías o heladas. Si tienes algún problema con tu lavadora, no dudes en llamarnos, somos tu mejor opción.</p>
              <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Enviar Mensaje</a>
                <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
              </div>
            </div>
          </div>


        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <div class="container marketing">

       <div class="row">

         <div class="col-12" style="text-align: center;">

          <!-- Start Alert Code -->
          <div class="toast sticky bg-primary text-white" data-autohide="false">
            <div class="toast-header">
              <strong class="mr-auto text-primary">Gran Oferta</strong>
              <small class="text-muted">Servicio en 2 hrs</small>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body">
              -10% de descuento. Contamos con las <span class="text-warning">medidas de sanidad necesarias</span> contra covid19. Aprovecha y agenda una visita<b><a href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="text-warning btn-web-action"> dando clic aqui</a>
                <a href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')" class="text-warning btn-mobile-action"> Agenda una visita dando clic aqui</a></b>
              </div>
            </div>
            <!-- End Alert Code -->
            <h2 class="featurette-heading-small">¿Porqué Contratarnos?
            </h2>
            <p class="lead">Porque contamos con más de 25 años de experiencia, ofrecemos precios justos, refacciones originales, diferentes métodos de pago y garantía por escrito.</p>
            <br><br>
          </div>
          <div class="col-lg-4 col-md-4 ">
            <img src="assets/img/garantia-servicio-frigidaire-electrolux.png" class="rounded" alt="reparacion de lavadoras" width="80px" >
            <h2>Garantía de Calidad</h2>
            <p>25 años de experiencia nos respaldan, somos expertos no charlatanes.</p>

          </div><!-- /.col-lg-4 -->

          <div class="col-lg-4 col-md-4 ">
            <img src="assets/img/servicio-a-domicilio-reparacion-de-refrigeradores-oferta.png" class="rounded" alt="reparacion de refrigeradores" width="80px">
            <h2>Precios Accecibles</h2>
            <p>Estamos conscientes de la situación actual, por ello te ofrecemos -10% de descuento.</p>

          </div>
          <!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-4">
            <img src="assets/img/piezas-originales-reparacion-de-estufas.png" class="rounded" alt="reparacion de refrigeradores" width="80px" >
            <h2>Refacciones Originales</h2>
            <p>Todas nuestras refacciones son originales, nosotros no fomentamos el uso de piratería.</p>

          </div><!-- /.col-lg-4 -->

        </div>
        <h2 class="featurette-heading text-center">Tenemos diferentes <span class="text-muted">métodos de pago</span></h2>
        <br>
        <div class="row text-center">
          <div class="col-md col-xs">
            <img src="assets/img/clip.png" class="rounded" alt="reparacion de lavadoras" width="60px" >
          </div>
          <div class="col-md col-xs">
            <img src="assets/img/bbva.png" class="rounded bbva" alt="reparacion de lavadoras" width="120px" >
          </div>
          <div class="col-md col-xs">
            <img src="assets/img/visa.png" class="rounded visa" alt="reparacion de lavadoras" width="110px" >
          </div>
          <div class="col-md col-xs">
            <img src="assets/img/mastercard.png" class="rounded" alt="reparacion de lavadoras" width="70px" >
          </div>
        </div>


        <br><br>
      </div>

      <section class="jumbotron text-center cta-black-cement">
        <div class="container marketing">
          <h2 class="featurette-heading-standard"><b>Gran cobertura</b></h2>
          <p class="lead">Actualmente tenemos cobertura en toda la CDMX y sus 16 Alcaldias <b>(Álvaro Obregón, Azcapotzalco, Benito Juárez, Coyoacán, Cuajimalpa de Morelos, Cuauhtémoc, Gustavo A. Madero, IztacalcoI, ztapalapa, Magdalena Contreras, Miguel Hidalgo, Milpa Alta, Tláhuac, Tlalpan, Venustiano Carranza, Xochimilco)</b> y Área Metropolitana.</p>
          <p>
            <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Whatsapp</a>
            <a class="btn btn-lg btn-block btn-success btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Llamar Ahora</a>
            <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
          </p>
        </div>
      </section>


      <div class="container">
        <br><br>
        <h2 class="featurette-heading">Agenda una <span class="text-muted">Visita</span></h2>
        <p class="lead">Recuerda que nuestros técnicos cuentan con más de 25 años de experiencia y muchos han trabajado directamente con <b>Samsung, Whirlpool, Maytag, Frigidaire, Bosch, Mabe, GE, LG, Torrey, Sub Zero, Hisense, Acros, York</b> y otras marcas. Solicita una visita a domicilio, contamos con los precios más accesibles del mercado. *No retiramos equipos del domicilio.</p>

        <div class="card-deck mb-3 text-center">
          <div class="card mb-4 bg-warning text-white shadow-sm">
            <div class="card-header">
              <h4 class="my-0 font-weight-normal">Reparación</h4>
            </div>
            <div class="card-body">
              <ul class="list-unstyled mt-3 mb-4">
                <li>Visita a domicilio</li>
                <li>Diagnostico final del técnico</li>
              </ul>
              <a type="button" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="btn btn-lg btn-block btn-outline-light btn-web-action">Obtén -10% de Desc</a>

              <a class="btn btn-lg btn-block btn-outline-light btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Obtén -10% de Desc</a>

            </div>
          </div>
          <div class="card mb-4 bg-secondary text-white shadow-sm">
            <div class="card-header">
              <h4 class="my-0 font-weight-normal">Mantenimiento</h4>
            </div>
            <div class="card-body">

              <ul class="list-unstyled mt-3 mb-4">
                <li>Visita a domicilio</li>
                <li>Diagnostico final del técnico</li>
              </ul>
              <a type="button" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="btn btn-lg btn-block btn-primary btn-web-action">Agendar Visita</a>
              <a class="btn btn-lg btn-block btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Llamar Ahora</a>
            </div>
          </div>
          <div class="card mb-4 bg-primary text-white shadow-sm">
            <div class="card-header">
              <h4 class="my-0 font-weight-normal">Instalación</h4>
            </div>
            <div class="card-body">

              <ul class="list-unstyled mt-3 mb-4">
                <li>Visita a domicilio</li>
                <li>Diagnostico final del técnico</li>
              </ul>
              <a type="button" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="btn btn-lg btn-block btn-success btn-web-action">Contáctanos</a>
              <a class="btn btn-lg btn-block btn-success btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Llamar Ahora</a>
            </div>
          </div>
        </div>
        <br><br>
      </div>

      <section class="jumbotron  cta-downgrey">
        <div class="row">
          <div class="col-md-8 text-right">
            <h2 class="featurette-heading-small"><b>Apoyamos tu economia, obtén un -10% de Descuento</b></h2>
            <p class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>refrigeradores, lavadoras, centros de lavado, hornos de cocción y estufas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b></p>
          </div>
          <div class="col-md-4 text-center">
            <div class="align-mobile">
              <a href="https://wa.me/525567757255" class="btn btn-primary mt shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Agendar Visita</a>
              <a class="btn btn-primary btn-lg shadow-lg btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Agendar Visita</a></p>
            </div>
            <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
          </div>
        </div>
      </section>

      <div class="container">
        <br><br><br class="hidde-element-mobile">
        <div class="row text-center">
          <div class="col-md-12"><h2 class="featurette-heading-small text-muted">Somos Expertos</h2>
            <p class="lead">Actualmente tenemos experiencia en la instalación, mantenimiento y reparación de Refrigeradores, Congeladores y Vitrinas Refrigeradas de diferentes marcas: <b>Frigidaire, Maytag, WOLF, Koblenz, Mabe, Samsung, LG, GE, Sub Zero, Whirlpool, Nieto, York, Torrey</b> y más...</p>
          </div>
          <div class="col-md-3 col-sm-6 col-xs"><img src="assets/img/frigidaire-logo.png" class="frigidaire" width="180px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs"><img src="assets/img/logo-maytag.png" class="maytag" width="125px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/whirlpool-logo.png" class="whirlpool" width="125px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/mabe-logo.png" class="mabe" width="120px" alt="reparacion de refrigeradores"></div>

          <div class="col-md-12 hidde-element-mobile"><br></div>

          <div class="col-md-3 col-sm-6 col-xs"><img src="assets/img/logo-sub-zero.png" class="koblenz" width="180px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/lg-logo.png" class="lg" width="90px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/wolf-logo.png" class="wolf" width="130px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/logo-samsung.png" class="samsung" width="130px" alt="reparacion de refrigeradores"></div>

          <div class="col-md-12 hidde-element-mobile"><br></div>

          <div class="col-md-3 col-sm-6 col-xs"><img src="assets/img/bosch.png" class="koblenz" width="150px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/ge.png" width="120px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/torrey.png" class="wolf" width="130px" alt="reparacion de refrigeradores"></div>
          <div class="col-md-3 col-sm-6 col-xs "><img src="assets/img/teka-logo.png" width="80px" alt="reparacion de refrigeradores"></div>

        </div>
        <br><br><br><br class="hidde-element-mobile">
      </div>

      <section class="jumbotron text-center cta-red-frigidaire">
        <div class="container marketing">
          <h2 class="featurette-heading-standard"><b>Agenda una visita a tu domicilio</b></h2>
          <p class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>refrigeradores, lavadoras, centros de lavado, hornos de cocción y estufas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b></p>
          <p>
            <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Whatsapp</a>
            <a class="btn btn-primary btn-lg btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Llamar Ahora</a></p>
            <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
          </p>
        </div>
      </section>

      <div class="container marketing">
       <br><br>
       <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Servicio <span class="text-muted">Urgente en Ciudad de México y Área Metropolitana.</span></h2>
          <p class="lead">Si tienes un refrigerador, una lavadora, un horno, una estufa o alguno de estos electrodomesticos descompuesto, nosotros te podemos ayudar a repararlo, somos un <b>Centro de Servicio Técnico especialistas en diferentes marcas, como: SAMSUNG, MAYTAG, MABE, FRIGIDAIRE, SUB ZERO, BOSCH, ELECTROLUX, WHIRLPOOL, HISENSE Y más...</b></p><br>
        </div>
        <div class="col-md-5 text-center" itemscope itemtype="http://schema.org/ImageObject" ><br><br>
          <img src="assets/img/reparacion-de-estufas-a-domicilio.png" alt="reparacion de lavadoras" itemprop="contentUrl" hidden> 
          <img class="img-size-png" src="assets/img/estufa-servicio-frigidaire.png" alt="reparacion de lavadoras"> 
        </div>
      </div>
      <br>
      <br><br>
    </div>

    <section class="jumbotron text-center cta-black-cement">
     <div class="container" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <h2 class="featurette-heading-small"><b>Servicio urgente en 2 hrs</b> </h2>
      <p class="lead"><span itemprop="description">Agendar una visita a domicilio con nuestro personal es muy sencillo, solo llamas, tomamos tus datos y te agendamos una visita, por ahora, en apoyo a tu economia te ofrecemos <span itemprop="availability"><b>un -10% de Descuento soló por este mes en todos nuestros servicios.</b></span></span></p>
      <p><a class="btn btn-primary btn-lg btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Mensaje</a>
        <a class="btn btn-primary btn-lg btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Llamar Ahora</a></p>
      </div>
    </section>


    <br><br>
    <div class="container ">
      <h2 class="featurette-heading">¿Estas interesado? <span class="text-muted">Solicita una Visita.</span></h2>
      <p class="lead">Si te interesa que un técnico te visite en tu domicilio, es simple, deja tus datos en el formulario. Si deseas comunicación directa, <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">manda un whatsapp.</a></b></p>
      <br>
      <form id="contact" class="needs-validation" novalidate>
        <div class="form-group">
          <label for="nombre">Nombre del Interesado</label>
          <input type="text" class="form-control" id="nombre" placeholder="ej: Juan Manual Rodriguez" name="nombre" minlength="4" maxlength="40" required pattern="^[a-zA-Z0-9 ]*$">
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un nombre correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
        </div>
        <div class="form-group">
          <label for="empresa">Nombre del Negocio ó Particular</label>
          <input type="text" class="form-control" id="empresa" placeholder="ej: MC Donals" name="empresa" minlength="5" maxlength="50" required pattern="^[a-zA-Z0-9 ]*$">
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un numero de empresa correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
        </div>
        <div class="form-group">
          <label for="servicio">Servicio de Interés</label>
          <select class="form-control" id="servicio" name="servicio" required>
            <option value="">Selecciona una opción...</option>
            <option>Visita a Domicilio</option>
            <option>Instalación</option>
            <option>Mantanimiento</option>
            <option>Reparación</option>
          </select>

          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Selecciona una opción...</div>
        </div>
        <div class="form-group">
          <label for="telefono">Teléfono</label>
          <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="ej: 5554344567" pattern="[0-9]{10}" required>
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un teléfono correcto, de 10 dígitos.</div>
        </div>
        <div class="form-group">
          <label for="correo">Correo</label>
          <input type="email" class="form-control" id="correo" placeholder="ej: contacto@paginaswebpremium.com.mx" name="correo" required>
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un correo correcto.</div>
        </div>
        <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="terms" required> Acepto los <span class="text-info" >Términos y Condiciones</span>.
            <div class="valid-feedback">Correcto.</div>
            <div class="invalid-feedback">Es necesario que aceptes los términos y condiciones para continuar.</div>
          </label>
        </div>
        <a onclick="sendForm(this)" class="btn btn-success text-white">Solicitar Cotización</a>
      </form>
    </div>

    <div class="modal fade" id="actionModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h2 class="modal-title"><p id="successModalTitle"></p></h2>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>
          <form>
            <!-- Modal body -->
            <div class="modal-body">
              <p id="successModalDescription"></p>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button class="btn btn-success" type="button" data-dismiss="modal" onclick="this.form.reset();">Continuar</button>
            </div>
          </form>
        </div>
      </div>
    </div>


    <div class="container">

      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <h5>Página Segura</h5>
            <img src="assets/img/ssl_comodo.png" width="125px" alt="reparacion de lavadoras" style="margin-bottom: 10px">
            <p class="text-small" style="margin-bottom: 10px">Todas las imágenes y logotipos son propiedad de la marca correspondiente.</p>
            <small class="d-block mb-3 text-muted">&copy; Ciudad de México 2020</small>
          </div>
          <div class="col-6 col-md">
            <h5>Nosotros</h5>
            <ul class="list-unstyled text-small">
              <li>Somos una empresa comprometida con nuestros clientes, ofrecemos precios justos, refacciones originales alta calidad.</li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Contacto</h5>
            <ul class="list-unstyled text-small">
              <li>
                <a class="text-muted btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')">Agendar una visita</a>
                <a class="text-muted btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'footcall', 'clic')">Agendar una visita</a>
              </li>
              <li><a class="text-muted btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')">Enviar correo directo</a><a class="text-muted btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'footcall', 'clic')">Enviar correo directo</a></li>
              <li><a class="text-muted btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')">Servicio Urgente</a><a class="text-muted btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'footcall', 'clic')">Servicio Urgente</a></li>
            </ul>
          </div>
        </div>
      </footer>
    </div>

    <div style=" position: fixed;" id="" class="myDiv sticky">
      <p id="tienes_dudas">
        <strong class="text-success">¿Necesitas ayuda?</strong>
        <a id="whats_link" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">
          <img src="https://tecnicosenrefrigeracioncdmx.com/assets/img/servicio-de-estufas-mensaje.png" alt="reparacion de refrigeradores" class="wp-image-31 alignnone size-medium" width="30" height="30" style="display: inline-block; margin-left:60px; margin-right: 10px;">
        </a>
        <a id="llamar_link"  href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">
          <img src="https://tecnicosenrefrigeracioncdmx.com/assets/img/reparacion-de-estufas-llamada.png" alt="reparacion de refrigeradores" class="wp-image-31 alignnone size-medium " width="30" height="30" style="display: inline-block;">
        </a>
      </p>
    </div>

  </main>


  <script type="text/javascript" src="assets/js/form.js" async=""></script>
  <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->

  <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>

  <script src="dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous" async></script>

  <script src="vendor/jquery-easing/jquery.easing.min.js" async></script>
</body>
</html>
