<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Servicio Frigidaire en Ciudad de México. Servicio Urgente a Domicilio en 2hrs. Centro de Servicio Frigidaire. Reparación, Mantenimiento e Instalación de Refrigeradores, Estufas, Lavadoras, Centros de Lavado, Hornos. Teléfono: 5534821512">
    <meta name="Keywords" content="Reparación de Refrigeradores Frigidaire, Reparación de Lavadoras Frigidaire, Reparación de Lavavajillas Frigidaire, Reparación de Secadoras Frigidaire, Servicio de Linea Blanca Frigidaire, Servicio de Refrigeradores Frigidaire, Servicio de Lavadoras Frigidaire, Servicio de Lavavajillas Frigidaire, Servicio Tecnico Frigidaire, Compostura de Refrigeradores, Reparación de Lavadoras, Reparación de Refrigeradores,Centro de Servicio Frigidaire, Servicio Frigidaire, Servicio Frigidaire Urgente, Servicio Frigidaire en CDMX">
    <meta name="title" content="Servicio Frigidaire en CDMX | Centro Autorizado | Servicio Urgente en CDMX | -10% de Descuento">
    <meta name="author" content="Páginas Web Premium">
    <meta name="Subject" content="Servicio Frigidaire en CDMX">
    <meta name="Language" content="es">
    <meta name="Revisit-after" content="15 days">
    <meta name="Distribution" content="Global">
    <meta name="Robots" content="Index, follow">
    <meta name="theme-color" content="#ee405a">
    <title>Servicio Frigidaire en CDMX | Centro Autorizado | -10% de Descuento</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link href="carousel.css" rel="stylesheet" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="assets/img/logo-servicio-frigidaire-en-cdmx.png" sizes="180x180">
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159219792-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-159219792-1');
    </script>

    <!-- Google Analytics Events-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-159219792-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

  </head>
  <body>
  <header class="sticky-top position"  >
  <div class="bg-red-frigidaire collapse" id="navbarHeader" style="">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Servicio Frigidaire a Domicilio</h4>
          <p class="text-white">Si se descompuso tu lavadora, estufa, horno, centro de lavado ó secadora, no dudes en llamar y pedir la visita de uno de nuestros técnicos en la comodidad de tu hogar.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Contacto</h4>
          <ul class="list-unstyled">
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'msjheader', 'clic')">Agendar una Visita</a></li>
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'msjheader', 'clic')">Servicio Urgente</a></li>
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'msjheader', 'clic')">Enviar Whatsapp</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-light bg-white shadow-sm ">
    <div class="container d-flex justify-content-between">
      <a href="#" class="navbar-brand d-flex align-items-center">
        <img src="assets/img/logo-servicio-frigidaire-en-cdmx.png" width="180px" alt="diseno-de-paginas-web">
      </a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon my-toggler"></span>
      </button>
    </div>
  </div>
</header>

<main role="main">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      <li data-target="#myCarousel" data-slide-to="3" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active pwp-background-first pwp-background-first-mobile">
        <div class="container ">
          <div itemscope itemtype="http://schema.org/Dataset"  class="carousel-caption text-left">
            <h2 class="tittle-h2"><b itemprop="name">Servicio Frigidaire</b></h2>
            <p class="text"><span itemprop="description">Contamos con técnicos especializados y servicio a dimicilio en la CDMX y Área Metropolitana, todos nuestros técnicos son capaces de resolver la problemática que tenga tu electrodoméstico lo mas rápido posible.</span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')" >Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525534821512" role="button" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a>
          </p>
          </div>
        </div> 
      </div>
      <div class="carousel-item pwp-background-second pwp-background-second-mobile">
        
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Reparación</b></h2>
            <p class="text">¿Como funciona?. Un técnico especializado te visita en tu hogar, realiza el análisis y te dice cuál es el problema y la solución, si se necesita comprar material, cuál es precio de la reparación y el tiempo que se tardaría en realizar el servicio. </p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525534821512" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-third pwp-background-third-mobile">
        <div class="container">
          <div class="carousel-caption text-right">
            <h2 class="tittle-h2"><b>Instalación</b></h2>
            <p class="text">Al contar con más de 20 años de experiencia brindamos servicios a Refrigeradores, Lavadoras, Lavasecadoras, Centros de Lavado, Lavavajillas y Secadoras, Cargas de Gas y Limpieza de Ductos.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525534821512" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-four pwp-background-four-mobile">
        <div class="container">
          <div class="carousel-caption text-left">
            <h2 class="tittle-h2"><b>Mantenimiento</b></h2>
            <p class="text">El funcionamiento de los refrigeradores se centra en un ciclo de refrigeración en constante movimiento, asi es que mantiene las cosas frías o heladas. Si tienes algún problema con tu lavadora, no dudes en llamarnos, somos tu mejor opción.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525534821512" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      
      
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Servicio Frigidaire <span class="text-muted">Urgente en CDMX y Área Metropolitana.</span></h2>
        <p class="lead">Si tienes un refrigerador, una lavadora, un horno, una estufa o alguno de estos electrodomesticos descompuesto, nosotros te podemos ayudar a repararlo, somos un <b>Centro de Servicio Autorizado Frigidaire en CDMX y Área Metropolitana.</b></p><br>
      </div>
      <div class="col-md-5 text-center" itemscope itemtype="http://schema.org/ImageObject" >
        
        <img class="img-size-png" src="assets/img/estufa-servicio-frigidaire.png" alt="diseno-de-paginas-web" itemprop="contentUrl"> 
      </div>
    </div>
    <br>
    <br><br>
  </div>
    
    <section class="jumbotron text-center cta-red-frigidaire">
    <div class="container marketing">
      <h2 class="featurette-heading-standard"><b>¿Tienes algún problema con tu refrigerador?</b></h2>
      <p class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>refrigeradores, lavadoras, centros de lavado, hornos de cocción y estufas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b></p>
      <p>
        <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">Enviar Whatsapp</a>
        <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
      </p>
    </div>
  </section>
    
    <br><br><br>

  
  <div class="container marketing">
    
    <div class="row">
       <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-small">¿Porqué Contratarnos?
      </h2>
      <p class="lead">Es simple, porque nosotros ofrecemos mejores <b>servicios en reparación, instalación y mantenimiento</b> de refrigeradores, lavadoras, centros de lavado, estufas y hornos de cocción, tanto de hogares como industriales.</p>
      <br><br>
    </div>

      <div class="col-lg-4 col-md-4 ">
        <img src="assets/img/garantia-servicio-frigidaire-electrolux.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Servicios de Calidad</h2>
        <p>Nuestros técnicos son expertos, sabemos lo que hacemos y damos buenos resultados.</p>
        
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4 col-md-4">
        <img src="assets/img/piezas-originales-servicio-frigidaire-electrolux.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Piezas Originales</h2>
        <p>Todas nuestras refacciones son originales de la marca Frigidaire, no a la pirateria.</p>
        
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4 col-md-4 ">
        <img src="assets/img/servicio-a-domicilio-frigidaire-electrolux.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Servicio a Domicilio</h2>
        <p>Con esto aseguramos que todos nuestros clientes quedan 100% satisfechos.</p>
        
    </div>
      <!-- /.col-lg-4 -->
    </div>
      <br><br>
    </div>

  <section class="jumbotron  cta-downgrey">
    <div class="row">
      <div class="col-md-8 text-right">
      <h2 class="featurette-heading-small"><b>¿Necesitas la visita de un técnico?</b></h2>
      <p class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>refrigeradores, lavadoras, centros de lavado, hornos de cocción y estufas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b></p>
    </div>
    <div class="col-md-4 text-center">
      <div class="align-mobile">
        <a href="https://wa.me/525567757255" class="btn btn-primary mt shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">Agendar Visita</a>
      </div>
        <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
    </div>
    </div>
  </section>
  
  <div class="container marketing">
    <br><br><br>
  <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading">¿No Funciona Bien tu <span class="text-muted">Refrigerador Frigidaire?</span></h2>
        <p class="lead">Nuestros técnicos están listos para reparar, dar mantenimiento o instalar una lavadora, un refrigerador, horno, estufa o centro de lavado. Recuerda que contamos con servicio a domicilio y registro en sistema  para seguimiento de garantías. Tenemos cobertura en CDMX y Área Metropolitana.</p>
      </div>
      <div class="col-md-5 order-md-1 text-center">
        <img class="img-size-png-frigo mt-img" src="assets/img/servicio-frigidaire-urgente-en-cdmx.jpg" alt="diseno-de-paginas-web" >
      </div>
  </div>
  <br><br><br>
  </div>
  <section class="jumbotron text-center cta-black-cement">
    <div class="container marketing">
      <h2 class="featurette-heading-standard"><b>¿En qué municipios hacemos visitas a domicilio?</b></h2>
      <p class="lead">Actualmente tenemos cobertura en toda la CDMX y sus 16 Alcaldias <b>(Álvaro Obregón, Azcapotzalco, Benito Juárez, Coyoacán, Cuajimalpa de Morelos, Cuauhtémoc, Gustavo A. Madero, IztacalcoI, ztapalapa, Magdalena Contreras, Miguel Hidalgo, Milpa Alta, Tláhuac, Tlalpan, Venustiano Carranza, Xochimilco)</b> y Área Metropolitana.</p>
      <p>
        <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">Enviar Whatsapp</a>
        <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
      </p>
    </div>
  </section>
  <!--
  <div class="row">
      <div class="col">
        <div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>

      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
  
      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div> 
  </div>
  -->
  <div class="container marketing">
  <br><br>
  <div class="row">

    <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-standard">Preguntas Frecuentes
      </h2>
      <p class="lead">A continuación listamos una pequeña serie de preguntas que hemos identificado como frecuentes o muy frecuentes, cualquier observación o comentario, no dudes y <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">envia un mensaje </a></b></p>
      <br><br>
    </div>


    <div class="col-md-4 col-sm-6">
      <div id="list-example" class="list-group">
        <a class="list-group-item list-group-item-action active disabled" href="#list-item-1">¿Qué precio tiene la visita a mi domicilio?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-2">¿El precio total incluye refacciones?<a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-3">¿Cual es su red de cobertura actúal?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-4">¿Me dan garatía del servicio?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-5">¿Qué metodos de pago aceptamos?</a>
      </div>
      <br>
    </div>
    <div class="col-md-8 col-sm-6 shadow-ask">
      <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
        <h4 id="list-item-1">¿Qué precio tiene la visita a mi domicilio?</h4>
        <p>La visita no tiene ningun costo, <b>siempre y  cuando acepte el presupuesto que nuestros técnicos le ofrezcan para la reparación de su electrodoméstico.</b> En caso contrario la cisita tiene un costo de $150 pesos en la CDMX y $350 pesos en el Estado de México.</p>
        <h4 id="list-item-2">¿El precio total incluye refacciones?</h4>
        <p>Asi es, el precio total incluye piezas y también te brindamos de paso una garantía que asegura la calidad y los buenos resultados de nuestros servicios.<b>Recuerda que contamos con servicio a domicilio</b> y si te animas a reparar tu equipo, la visita no tiene costo, es completamente gratis.</p>
        <h4 id="list-item-3">¿Cual es su red de cobertura actúal?</h4>
        <p>Actualmente tenemos cobertura en toda la CDMX y sus 16 Alcaldias <b>(Álvaro Obregón, Azcapotzalco, Benito Juárez, Coyoacán, Cuajimalpa de Morelos, Cuauhtémoc, Gustavo A. Madero, IztacalcoI, ztapalapa, Magdalena Contreras, Miguel Hidalgo, Milpa Alta, Tláhuac, Tlalpan, Venustiano Carranza, Xochimilco)</b> y Área Metropolitana.</p>
        <h4 id="list-item-4">¿Me dan garatía del servicio?</h4>
        <p>Claro, La garantía te la entregamos en papel, con un formato impreso y también con un registro que llevamos de nuestros servicios en sistema.</b> Entonces tu garantía estará disponible también en la bandeja de entrada de tu correo personal si asi lo deseas.</b></p>
        <h4 id="list-item-5">¿Que métodos de pago aceptamos?</h4>
        <p>Actualmente recibimos todas las tarjetas de <b>Credito y Débito</b>. Por mencionar algunas: <b>VISA, AMERICAN EXPRESS, MASTER CARD, MAESTRO y más...</b> La idea principal es tener la oportunidad de trabajar contigo y abrir el catálogo de oportunidades que tenemos disponibles para ti.</p>
      </div>
    </div>
    <div class="col-12"> <br><br><br></div>
   
  </div>
  </div>
  
  <section class="jumbotron text-center cta-red-frigidaire">
       <div class="container" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
          <h2 class="featurette-heading-small"><b>Agendar una Visita a mi Domicilio</b> </h2>
          <p class="lead"><span itemprop="description">Agendar una visita a domicilio con nuestro personal es muy sencillo, solo llamas, tomamos tus datos y te agendamos una visita, por ahora, te ofrecemos <span itemprop="availability"><b>15% de Descuento soló por este mes en todos nuestros servicios.</b></span></span></p>
          <p><a class="btn btn-primary btn-lg btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">Enviar Mensaje</a>
    <a class="btn btn-primary btn-lg btn-mobile-action" href="tel:+525534821512" onclick="ga('send', 'event', 'inicio', 'hacerllamada', 'clic')">Llamar Ahora</a></p>
      </div>
  </section>


      <br><br>
  <div class="container ">
    <h2 class="featurette-heading">¿Estas interesado? <span class="text-muted">Solicita una Visita.</span></h2>
    <p class="lead">Si te interesa que un técnico te visite en tu domicilio, es simple, deja tus datos en el formulario. Si deseas comunicación directa, <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'enviarwhats', 'clic')">manda un whatsapp.</a></b></p>
    <br>
    <form id="contact" class="needs-validation" novalidate>
    <div class="form-group">
      <label for="nombre">Nombre del Interesado</label>
      <input type="text" class="form-control" id="nombre" placeholder="ej: Juan Manual Rodriguez" name="nombre" minlength="4" maxlength="40" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un nombre correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="empresa">Nombre del Negocio ó Particular</label>
      <input type="text" class="form-control" id="empresa" placeholder="ej: MC Donals" name="empresa" minlength="5" maxlength="50" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un numero de empresa correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="servicio">Servicio de Interés</label>
      <select class="form-control" id="servicio" name="servicio" required>
        <option value="">Selecciona una opción...</option>
        <option>Visita a Domicilio</option>
        <option>Instalación</option>
        <option>Mantanimiento</option>
        <option>Reparación</option>
      </select>
      
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Selecciona una opción...</div>
    </div>
    <div class="form-group">
      <label for="telefono">Teléfono</label>
      <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="ej: 5554344567" pattern="[0-9]{10}" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un teléfono correcto, de 10 dígitos.</div>
    </div>
    <div class="form-group">
      <label for="correo">Correo</label>
      <input type="email" class="form-control" id="correo" placeholder="ej: contacto@paginaswebpremium.com.mx" name="correo" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un correo correcto.</div>
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="terms" required> Acepto los <span class="text-info" >Términos y Condiciones</span>.
        <div class="valid-feedback">Correcto.</div>
        <div class="invalid-feedback">Es necesario que aceptes los términos y condiciones para continuar.</div>
      </label>
    </div>
    <a onclick="sendForm(this)" class="btn btn-success text-white">Solicitar Cotización</a>
  </form>
  <br><br><br>
  </div>

</main>

<div style=" position: fixed;" id="" class="myDiv sticky">
  <p id="tienes_dudas">
    <strong class="text-success">¿Necesitas ayuda?</strong>
    <a class="whats-cta-sticky" id="whats_link" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'frigidaire', 'whats', 'clic')">
      <img src="assets/img/reparacion-de-estufas-a-domicilio-mensaje.png" alt="" class="wp-image-31 alignnone size-medium" width="30" height="30" style="display: inline-block;">
    </a>
    <a class="call-cta-sticky" id="llamar_link" href="tel:+525534821512" onclick="ga('send', 'event', 'frigidaire', 'llamada', 'clic')">
      <img src="assets/img/reparacion-de-estufas-a-domicilio-llamada.png" alt="" class="wp-image-31 alignnone size-medium " width="30" height="30" style="display: inline-block; margin-right:20px;">
    </a>
  </p>
</div>


<!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal" onclick="this.form.reset();">Continuar</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<footer class="text-muted bg-black-cement" style="color: white !important; ">
  <div class="container">
    <p class="float-right">
      <a href="#" class="text-warning">Volver al Principio</a>
    </p>
    <p>Frigidaire México | PROTECT DATA WITH COMODO SECURE&trade; – <b class="text-success">RapidSSL®</b></p>
    <p class="text-black-cement">Powered by <a class="text-black-cement" href="https://paginaswebpremium.com.mx">Páginas Web Premium</a></p>
  </div>
</footer>

  <script type="text/javascript" src="assets/js/letters.js"></script>
  <script type="text/javascript" src="assets/js/form.js"></script>
  <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
        
  <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>

  <script src="dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>
